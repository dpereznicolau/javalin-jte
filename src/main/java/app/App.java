package app;

import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import io.javalin.http.Context;

import java.util.Collections;

public class App {
    public static void main(String[] args) {
        Javalin app = Javalin.create(App::configure).start(7000);

        app.get("/", App::renderHelloPage);
    }

    private static void configure(JavalinConfig config) {
        config.addStaticFiles("/public");
    }

    private static void renderHelloPage(Context ctx) {
        HelloPage page = new HelloPage();
        page.userName = "admin";
        page.userKarma = 1337;
        ctx.render("hello.jte", Collections.singletonMap("page", page));
    }
}